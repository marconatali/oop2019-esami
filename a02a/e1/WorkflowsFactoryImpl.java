package a02a.e1;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class WorkflowsFactoryImpl implements WorkflowsFactory {

	@Override
	public <T> Workflow<T> singleTask(T task) {
		return new Workflow<T>() {

			private Set<T> tasks = new HashSet<T>(Set.of(task));
			@Override
			public Set<T> getTasks() {
				return Set.of(tasks.iterator().next());
			}

			@Override
			public Set<T> getNextTasksToDo() {
				return tasks;
			}

			@Override
			public void doTask(T t) {
				tasks.remove(t);
			}

			@Override
			public boolean isCompleted() {
				return tasks.isEmpty();
			}
		};
	}

	@Override
	public <T> Workflow<T> tasksSequence(List<T> tasks) {
		return new Workflow<T>() {

			private Set<T> task = tasks.stream().collect(Collectors.toSet());
			@Override
			public Set<T> getTasks() {
				return task;
			}

			@Override
			public Set<T> getNextTasksToDo() {
				if(!isCompleted()) {
					return Set.of(task.stream().findFirst().get());
				}
				return new HashSet<T>();
			}

			@Override
			public void doTask(T t) {
				this.task.remove(t);
			}

			@Override
			public boolean isCompleted() {
				return task.isEmpty();
			}
		};
	}

	@Override
	public <T> Workflow<T> tasksJoin(Set<T> initialTasks, T finalTask) {
		return new Workflow<T>() {

			private Set<T> tasks = new HashSet<T>(initialTasks);
			private Set<T> finTasks = new HashSet<T>(Set.of(finalTask));
			@Override
			public Set<T> getTasks() {
				Set <T> res = new HashSet<T>(tasks);
				res.addAll(finTasks);
				return res;
			}

			@Override
			public Set<T> getNextTasksToDo() {
				if(!isCompleted() && !tasks.isEmpty()) {
					return tasks;
				}
				return finTasks;
			}

			@Override
			public void doTask(T t) {
				if(tasks.isEmpty()) {
					finTasks.remove(t);
				} else {
					tasks.remove(t);
				}				
			}

			@Override
			public boolean isCompleted() {
				return this.tasks.isEmpty() && this.finTasks.isEmpty();
			}
		};
	}

	@Override
	public <T> Workflow<T> tasksFork(T initialTask, Set<T> finalTasks) {
		return new Workflow<T>() {

			private Set<T> tasks = new HashSet<T>(finalTasks);
			private Set<T> initTasks = new HashSet<T>(Set.of(initialTask));
			@Override
			public Set<T> getTasks() {
				Set <T> res = new HashSet<T>();				
				res.addAll(initTasks);
				res.addAll(tasks);
				return res;
			}

			@Override
			public Set<T> getNextTasksToDo() {
				if(!isCompleted() && !initTasks.isEmpty()) {
					return initTasks;
				}
				return tasks;
			}

			@Override
			public void doTask(T t) {
				if(initTasks.isEmpty()) {
					tasks.remove(t);
				} else {
					initTasks.remove(t);
				}				
			}

			@Override
			public boolean isCompleted() {
				return this.tasks.isEmpty() && this.initTasks.isEmpty();
			}
		};
	}

	@Override
	public <T> Workflow<T> concat(Workflow<T> first, Workflow<T> second) {
		// TODO Auto-generated method stub
		return null;
	}

}
