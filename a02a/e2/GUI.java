package a02a.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    private static final long serialVersionUID = -6218820567019985015L;
    private Map<JButton, Pair<Integer, Integer>> buttons;
    private Logic logic;
    
    public GUI(int size) {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        
        ActionListener ap = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic.hit();
            draw();
        };
        
        JButton play = new JButton("PLAY");
        play.addActionListener(ap);
        this.getContentPane().add(BorderLayout.SOUTH,play);
        
        buttons=new HashMap<>();
        logic=new LogicImpl(size);
        
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            if(logic.isOver(buttons.get(bt).getX(), buttons.get(bt).getY())) {
                System.exit(1);
            }
        };
        
        for (int i=0;i<size;i++){
        	for(int j=0;j<size;j++) {
        		final JButton jb = new JButton();
                jb.addActionListener(al);
                panel.add(jb);
                buttons.put(jb, new Pair<>(i,j));
        	}
        } 
        this.setVisible(true);
    }
    
    private void draw() {
    	List<Pair<Integer,Integer>> list = logic.getList();
    	for(int i=0;i<list.size();i++) {
    		for(JButton jb : buttons.keySet()) {
    			String text = buttons.get(jb) == list.get(i) ? "X" : "";
    			jb.setText(text);
    		}
    	}
    }
    
    
}
