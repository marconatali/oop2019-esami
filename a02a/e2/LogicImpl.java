package a02a.e2;

import java.util.LinkedList;
import java.util.List;

import a02a.e2.Logic;

public class LogicImpl implements Logic {

	private int size;
	private boolean dir;
	int xVal;
	int yVal;
	//private List<Pair<Integer,Integer>> grid = new LinkedList();
	private List<Pair<Integer,Integer>> grid;
	
	public LogicImpl(int size) {
		dir = false;
		xVal=0;
		yVal=0;
		this.size=size;
		initialize(xVal,yVal);
	}
	
	private void initialize(int i,int j) {
		grid=new LinkedList<Pair<Integer,Integer>>();
		for(; i<size; i++) {
			for(;j<size;j++) {
				if(i==size-1-i || i == 0 || i == (size - i - 1)/2 && j==0 || j==size-1-j) {
					grid.add(new Pair<>(i, j));
				}if(j == (size - j - 1)/2 && i==0 || i == size-1-i) {
					grid.add(new Pair<>(i, j));
				}
			}
		}
	}

	@Override
	public void hit() {
		setDir();
		xVal = dir ? xVal-2 : xVal + 2;
		yVal = dir ? yVal-2 : yVal + 2;
		initialize(xVal, yVal);
	}

	private void setDir() {
		if(xVal==size/2 && yVal == size/2) {
			dir=true;
		}
		if(xVal==0 && yVal == 0) {
			dir=false;
		}
	}
	
	public List<Pair<Integer, Integer>> getList(){
		return grid;
	}

	@Override
	public boolean isOver(int x, int y) {
		return grid.contains(new Pair<>(x,y));
	}
}
