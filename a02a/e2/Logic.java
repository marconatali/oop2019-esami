package a02a.e2;

import java.util.List;

public interface Logic {
	void hit();
	boolean isOver(int x, int y);
	List<Pair<Integer,Integer>> getList();
}
