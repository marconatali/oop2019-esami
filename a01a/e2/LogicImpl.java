package a01a.e2;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class LogicImpl implements Logic {

	private static final int MAX_ATTEMPTS=5;
	private Set<Pair<Integer,Integer>> boat;
	int nAttempts;
	int size;
	
	
	
	public LogicImpl(int size,int boatLength) {
		this.boat=new HashSet<>();
		this.size = size;
		this.nAttempts=MAX_ATTEMPTS;
		this.initializeBoat(boatLength);
	}

	private void initializeBoat(int boatLength) {
		Random r=new Random();
		Pair<Integer,Integer> initPos=new Pair<>(r.nextInt(size), r.nextInt(size-boatLength));
		for(int i=0;i<boatLength;i++) {
			this.boat.add(new Pair<>(initPos.getX(),initPos.getY()+i));
		}
		
	}

	@Override
	public boolean hit(int x, int y) {
		nAttempts--;
		return boat.remove(new Pair<>(x, y));
	}

	@Override
	public boolean isOver() {
		return nAttempts==0 || boat.isEmpty();
	}

	@Override
	public boolean getResult() {
		return  boat.isEmpty();
	}

}
