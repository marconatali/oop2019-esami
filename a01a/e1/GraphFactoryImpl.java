package a01a.e1;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GraphFactoryImpl implements GraphFactory {

	@Override
	public <X> Graph<X> createDirectedChain(List<X> nodes) {
		return new Graph<X>() {
			Set<X> set=nodes.stream().collect(Collectors.toSet());
			@Override
			public Set<X> getNodes() {
				return set;
			}

			@Override
			public boolean edgePresent(X start, X end) {
				return !start.equals(end);
			}

			@Override
			public int getEdgesCount() {
				return (set.size()-1);
			}

			@Override
			public Stream<Pair<X, X>> getEdgesStream() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public <X> Graph<X> createBidirectionalChain(List<X> nodes) {
		return new Graph<X>() {
			Set<X> set=nodes.stream().collect(Collectors.toSet());
			@Override
			public Set<X> getNodes() {
				return set;
			}

			@Override
			public boolean edgePresent(X start, X end) {
				return !start.equals(end);
			}

			@Override
			public int getEdgesCount() {
				return (set.size()-1)*2;
			}

			@Override
			public Stream<Pair<X, X>> getEdgesStream() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public <X> Graph<X> createDirectedCircle(List<X> nodes) {
		return new Graph<X>() {
			Set<X> set=nodes.stream().collect(Collectors.toSet());
			@Override
			public Set<X> getNodes() {
				return set;
			}

			@Override
			public boolean edgePresent(X start, X end) {
				return !start.equals(end);
			}

			@Override
			public int getEdgesCount() {
				return set.size();
			}

			@Override
			public Stream<Pair<X, X>> getEdgesStream() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public <X> Graph<X> createBidirectionalCircle(List<X> nodes) {
		return new Graph<X>() {
			Set<X> set=nodes.stream().collect(Collectors.toSet());
			@Override
			public Set<X> getNodes() {
				return set;
			}

			@Override
			public boolean edgePresent(X start, X end) {
				return !start.equals(end);
			}

			@Override
			public int getEdgesCount() {
				return set.size()*2;
			}

			@Override
			public Stream<Pair<X, X>> getEdgesStream() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public <X> Graph<X> createDirectedStar(X center, Set<X> nodes) {
		Set<X> set=nodes.stream().collect(Collectors.toSet());
		set.add(center);
		return new Graph<X>() {
			
			@Override
			public Set<X> getNodes() {
				
				return set;
			}

			@Override
			public boolean edgePresent(X start, X end) {
				return !start.equals(end);
			}

			@Override
			public int getEdgesCount() {
				return nodes.size();
			}

			@Override
			public Stream<Pair<X, X>> getEdgesStream() {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

	@Override
	public <X> Graph<X> createBidirectionalStar(X center, Set<X> nodes) {
		Set<X> set=nodes.stream().collect(Collectors.toSet());
		set.add(center);
		return new Graph<X>() {
			
			@Override
			public Set<X> getNodes() {				
				return set;
			}

			@Override
			public boolean edgePresent(X start, X end) {
				return !start.equals(end);
			}

			@Override
			public int getEdgesCount() {
				return (set.size()-1)*2;
			}

			@Override
			public Stream<Pair<X, X>> getEdgesStream() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public <X> Graph<X> createFull(Set<X> nodes) {
		return new Graph<X>() {
			Set<X> set=nodes.stream().collect(Collectors.toSet());
			@Override
			public Set<X> getNodes() {
				return set;
			}

			@Override
			public boolean edgePresent(X start, X end) {
				return !start.equals(end);
			}

			@Override
			public int getEdgesCount() {
				return set.size()*2;
			}

			@Override
			public Stream<Pair<X, X>> getEdgesStream() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public <X> Graph<X> combine(Graph<X> g1, Graph<X> g2) {
		// TODO Auto-generated method stub
		return null;
	}

}
