package a02b.e1;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ExamsFactoryImpl implements ExamsFactory {

	@Override
	public CourseExam<SimpleExamActivities> simpleExam() {
		return new CourseExam<SimpleExamActivities>() {
			private List<SimpleExamActivities> exam = new LinkedList<>(List.of(ExamsFactory.SimpleExamActivities.WRITTEN,
					ExamsFactory.SimpleExamActivities.ORAL,
					ExamsFactory.SimpleExamActivities.REGISTER));
			@Override
			public Set<SimpleExamActivities> getPendingActivities() {
				if(!examOver()) {
					return Set.of(exam.get(0));
				}
				return new HashSet<>();
			}
			
			@Override
			public boolean examOver() {
				return exam.isEmpty();
			}
			
			@Override
			public void completed(SimpleExamActivities a) {
				this.exam.remove(a);
			}
			
			@Override
			public Set<SimpleExamActivities> activities() {
				return exam.stream().collect(Collectors.toSet());
			}
		};
	}

	@Override
	public CourseExam<OOPExamActivities> simpleOopExam() {
		return new CourseExam<ExamsFactory.OOPExamActivities>() {
			
			private List<OOPExamActivities> exam = new LinkedList<>(List.of(ExamsFactory.OOPExamActivities.LAB_REGISTER,
					ExamsFactory.OOPExamActivities.LAB_EXAM));
			
			private List<OOPExamActivities> project = new LinkedList<>(List.of(OOPExamActivities.PROJ_PROPOSE,
					OOPExamActivities.PROJ_SUBMIT,
					OOPExamActivities.PROJ_EXAM));
			
			private List<OOPExamActivities> fin = new LinkedList<>(List.of(OOPExamActivities.FINAL_EVALUATION));
			
			@Override
			public Set<OOPExamActivities> getPendingActivities() {
				if(examOver()) {
					return new HashSet<>();
				}			
				if(!exam.isEmpty()) {
					return Set.of(exam.get(0),project.get(0));
				} else if (!project.isEmpty()){
					return (Set.of(project.get(0)));
				}				
				return Set.of(fin.get(0));				
			}
			
			@Override
			public boolean examOver() {
				return fin.isEmpty();
			}
			
			@Override
			public void completed(OOPExamActivities a) {
				exam.remove(a);
				project.remove(a);
				fin.remove(a);
			}
			
			@Override
			public Set<OOPExamActivities> activities() {
				Set<OOPExamActivities> res = new HashSet<>();
				res.addAll(exam);
				res.addAll(project);
				res.addAll(fin);
				return res;
			}
		};
	}

	@Override
	public CourseExam<OOPExamActivities> fullOopExam() {
		// TODO Auto-generated method stub
		return null;
	}

}
