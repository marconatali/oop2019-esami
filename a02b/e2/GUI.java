package a02b.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    private static final long serialVersionUID = -6218820567019985015L;
    private Map<JButton,Pair<Integer, Integer>> buttons;
    private Logic logic;
    
    public GUI(int size) {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
   
        buttons=new HashMap<JButton, Pair<Integer,Integer>>();
        logic=new LogicImpl(size);
        
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        JButton south = new JButton("South");
        this.getContentPane().add(BorderLayout.SOUTH,south);
        
        ActionListener as = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic.update();
            draw();
        };
        
        south.addActionListener(as);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic.hit(buttons.get(bt).getX(), buttons.get(bt).getY());
            draw();
        };
        
        for (int i=0;i<size;i++){
        	for(int j=0;j<size;j++) {
        		 final JButton jb = new JButton();
                 jb.addActionListener(al);
                 panel.add(jb);
                 buttons.put(jb, new Pair<>(i,j));
        	}
           
        } 
        this.setVisible(true);
    }

	private void draw() {
		for(JButton bt : buttons.keySet()) {
			if(logic.hasX(buttons.get(bt).getX(), buttons.get(bt).getY())) {
				bt.setText("X");
			} else {
				bt.setText("");
			}
		}
	}
    
}
