package a02b.e2;

public interface Logic {
	void hit(int x, int y);
	void update();
	boolean hasX(int x , int y);
}
