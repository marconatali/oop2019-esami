package a02b.e2;

import java.util.HashMap;
import java.util.Map;

public class LogicImpl implements Logic {

	private Map<Pair<Integer,Integer>,Boolean> grid;
	private int size;
	
	public LogicImpl(int size) {
		grid=new HashMap<>();
		this.size=size;
	}
	
	@Override
	public void hit(int x, int y) {
		grid.put(new Pair<>(x, y), true);
	}

	@Override
	public void update() {
		Map<Pair<Integer,Integer>,Boolean> newGrid=new HashMap<>();
		for(Pair<Integer,Integer> p:grid.keySet()) {
			if(grid.get(p)) {
				if(p.getX()+1<size) {
					newGrid.put(new Pair<>(p.getX()+1,p.getY()), true);
				}else {
					newGrid.put(new Pair<>(p.getX()-1,p.getY()), false);
				}				
			} else {
				if(p.getX()>0) {
					newGrid.put(new Pair<>(p.getX()-1,p.getY()), false);
				} else {
					newGrid.put(new Pair<>(p.getX()+1,p.getY()), true);
				}
			}
		}		
		grid=new HashMap<>(newGrid);
	}

	@Override
	public boolean hasX(int x, int y) {
		return this.grid.containsKey(new Pair<>(x, y));
	}

}
