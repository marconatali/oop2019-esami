package a01b.e2;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class LogicImpl implements Logic {

	private final Set<Pair<Integer,Integer>> mines;
	private final int size;
	private int contNoMines;
	
	
	public LogicImpl(int size,int nMines) {
		mines=new HashSet<>();
		contNoMines=size*size-nMines;
		this.size = size;
		initializeField(nMines);
	}

	private void initializeField(int nMines) {
		Random r = new Random();
		mines.add(new Pair<>(r.nextInt(size), r.nextInt(size)));
		Pair<Integer,Integer> mine = new Pair<>(r.nextInt(size),r.nextInt(size));
		while(mines.contains(mine)) {
			mine = new Pair<>(r.nextInt(size),r.nextInt(size));
		}
		mines.add(mine);
		System.out.println(mines);
	}

	@Override
	public boolean getResult(){
		return contNoMines==1;
	}

	@Override
	public boolean hit(int x, int y) {		
		if(!mines.contains(new Pair<>(x,y)) && contNoMines!=1) {
			contNoMines--;
			return false;
		}
		return true;
	}

	@Override
	public int getAdjacent(int x, int y) {
		int cont=0;
		for(int i=Math.max(0, x-1);i<=Math.min(size-1, x+1);i++) {
			for(int j=Math.max(0,y-1);j<=Math.min(size-1, y+1);j++) {
				if(mines.contains(new Pair<>(i,j))){
					cont++;
				}
			}
		}
		return cont;
	}

}
