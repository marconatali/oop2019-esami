package a01b.e1;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class GridFactoryImpl implements GridFactory {

	@Override
	public <E> Grid<E> create(int rows, int cols) {
		
		List<Cell<E>> grid= new LinkedList<Cell<E>>();
		for(int i=0;i<rows;i++) {
			for(int j=0;j<cols;j++) {
				grid.add(new Cell<E>(i,j, null));
			}
		}
		return new Grid<E>() {
			
			@Override
			public int getRows() {
				return rows;
			}

			@Override
			public int getColumns() {
				return cols;
			}

			@Override
			public E getValue(int row, int column) {
				return grid.stream().filter(e->e.getRow()==row && e.getColumn()==column).findFirst().get().getValue();
			}

			@Override
			public void setColumn(int column, E value) {
				grid.removeIf(e->e.getColumn()==column);
				for(int i=0;i<this.getRows();i++) {
					grid.add(new Cell<E>(i, column, value));
				}
			}

			@Override
			public void setRow(int row, E value) {
				grid.removeIf(e->e.getRow()==row);
				for(int i=0;i<this.getColumns();i++) {
					grid.add(new Cell<E>(row, i, value));
				}
				
			}

			@Override
			public void setBorder(E value) {
				this.setRow(0, value);
				this.setRow(rows-1, value);
				this.setColumn(0, value);
				this.setColumn(cols-1, value);
			}

			@Override
			public void setDiagonal(E value) {
				grid.removeIf(c->c.getRow()-c.getColumn()==0);
				for(int i=0;i<Math.min(rows, cols);i++) {
					grid.add(new Cell<E>(i, i, value));
				}
			}

			@Override
			public Iterator<Cell<E>> iterator(boolean onlyNonNull) {
				return grid.stream()
						.sorted((c1,c2)->c1.getRow() == c2.getRow() ? 
								c1.getColumn()-c2.getColumn() 
								: c1.getRow()-c2.getRow() )
						.filter(c->c.getValue() != null || !onlyNonNull).iterator();
			}
		};
	}

}
